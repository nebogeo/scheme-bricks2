#include <QtGui>
#include <iostream>

#include "drag_widget.h"

using namespace std;

drag_widget::drag_widget(QWidget *parent)
  : QWidget(parent)
{
  setAcceptDrops(true);
}

void drag_widget::dragEnterEvent(QDragEnterEvent *event)
{
  if (event->mimeData()->hasFormat("application/x-schemebricks")) {
    if (children().contains(event->source())) {
      event->setDropAction(Qt::MoveAction);
      event->accept();
    } else {
      event->acceptProposedAction();
    }
  } else if (event->mimeData()->hasText()) {
    event->acceptProposedAction();
  } else {
    event->ignore();
  }
}

void drag_widget::dragMoveEvent(QDragMoveEvent *event)
{
  if (event->mimeData()->hasFormat("application/x-schemebricks")) {
    if (children().contains(event->source())) {
      event->setDropAction(Qt::MoveAction);
      event->accept();
    } else {
      event->acceptProposedAction();
    }
  } else if (event->mimeData()->hasText()) {
    event->acceptProposedAction();
  } else {
    event->ignore();
  }
}

void drag_widget::dropEvent(QDropEvent *event)
{
  if (event->mimeData()->hasFormat("application/x-schemebricks")) {
    const QMimeData *mime = event->mimeData();
    QByteArray itemData = mime->data("application/x-schemebricks");
    QDataStream dataStream(&itemData, QIODevice::ReadOnly);
    QString text;
    QPoint offset;
    dataStream >> text >> offset;
    add(text.toUtf8().constData(),event->pos(), offset);

    if (event->source() == this) {
      event->setDropAction(Qt::MoveAction);
      event->accept();
    } else {
      event->acceptProposedAction();
    }
  } else {
    event->ignore();
  }
}
