#include <QtGui>
#include <QString>
#include <string>
#include <iostream>
#include "brick_widget.h"
#include "sexpr.h"

using namespace std;

brick_widget::brick_widget(QWidget *parent)
  : drag_widget(parent),
    m_delete_me(false)
{
}


void brick_widget::mousePressEvent(QMouseEvent *event)
{
  QPoint hotSpot = event->pos();
  QByteArray itemData;
  QDataStream dataStream(&itemData, QIODevice::WriteOnly);
  cerr<<"sent ["<<code()<<"]"<<endl;
  dataStream << QString::fromStdString(code()) << event->pos();
  QMimeData *mimeData = new QMimeData;
  mimeData->setData("application/x-schemebricks", itemData);
  //mimeData->setText(child->code());

  QDrag *drag = new QDrag(this);
  drag->setMimeData(mimeData);
  drag->setPixmap(QPixmap::grabWidget(this));
  drag->setHotSpot(hotSpot);
  hide();
  drag->exec(); //Qt::MoveAction | Qt::CopyAction, Qt::CopyAction);
  m_delete_me = true;
}
