 #ifndef DRAGWIDGET_H
 #define DRAGWIDGET_H

#include <QWidget>
#include <string>

 class QDragEnterEvent;
 class QDropEvent;

 class drag_widget : public QWidget
 {
 public:
     drag_widget(QWidget *parent = 0);
     virtual void add(const std::string &text, QPoint pos, QPoint offset)=0;

 protected:
     void dragEnterEvent(QDragEnterEvent *event);
     void dragMoveEvent(QDragMoveEvent *event);
     virtual void dropEvent(QDropEvent *event);
 };

 #endif
