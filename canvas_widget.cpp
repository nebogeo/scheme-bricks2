#include <QtGui>

#include "canvas_widget.h"
#include "brick_widget.h"
#include "brick_atom_widget.h"
#include "brick_list_widget.h"
#include "sexpr.h"
#include <iostream>

using namespace std;

canvas_widget::canvas_widget(QWidget *parent)
  : drag_widget(parent)
{
  setWindowTitle(tr("scheme bricks"));
}

void canvas_widget::add(const string &text, QPoint pos, QPoint offset) {
  brick_widget *w = build_from_code(this,text);
  cerr<<"built "<<text<<endl;
  w->move(pos - offset);
  //addWidget(newLabel);
  w->show();
  w->setAttribute(Qt::WA_DeleteOnClose);
}

// assume well formatted atm
brick_widget *canvas_widget::build_from_code(drag_widget *parent, const string &code) {
  if (code[0]=='(') {
    brick_list_widget *w = new brick_list_widget(parent);

    // skip (
    string inner = code.substr(1,sexpr::count_paren(code)-1);
    vector<string> tokens = sexpr::tokenise(inner);
    cerr<<"inner-: "<<inner<<endl;
    for (vector<string>::iterator i=tokens.begin(); i!=tokens.end(); ++i) {
      cerr<<"token-: "<<*i<<endl;
      w->add(build_from_code(w,*i));
    }
    return w;

  } else {
    brick_atom_widget *w = new brick_atom_widget(code,parent);
    return w;
  }

}

void canvas_widget::mousePressEvent(QMouseEvent *event)
{
  brick_widget *child = static_cast<brick_widget*>(childAt(event->pos()));
  if (!child)
    return;

  QPoint hotSpot = event->pos() - child->pos();

  QByteArray itemData;
  QDataStream dataStream(&itemData, QIODevice::WriteOnly);
  dataStream << QString::fromStdString(child->code()) << QPoint(hotSpot);

  QMimeData *mimeData = new QMimeData;
  mimeData->setData("application/x-schemebricks", itemData);
  //mimeData->setText(child->labelText());

  QDrag *drag = new QDrag(this);
  drag->setMimeData(mimeData);
  drag->setPixmap(QPixmap::grabWidget(child));
  drag->setHotSpot(hotSpot);

  child->hide();

  if (drag->exec(Qt::MoveAction | Qt::CopyAction, Qt::CopyAction) == Qt::MoveAction)
    child->close();
  else
    child->show();
}
