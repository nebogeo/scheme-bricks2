 #include <QApplication>
 #include <QMainWindow>
 #include <QPoint>
 #include "brick_widget.h"
 #include "canvas_widget.h"

int main(int argc, char *argv[]) {
  Q_INIT_RESOURCE(schemebricks);

  QApplication app(argc, argv);

  canvas_widget window;
  window.resize(640, 480);


  //brick_widget root("root",&window);
  //root.set_depth(0);
  //brick_widget::m_root=&root;

  //root.move(100,100);
  //root.resize(500, 200);

  window.add("(play-now (sine (add 100 (mul (add (sine 1.3) (sine 1)) 100))) 0)",
             QPoint(100,100),QPoint(0,0));

  window.show();

  return app.exec();
}
