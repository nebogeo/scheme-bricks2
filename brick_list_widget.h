#ifndef BRICKLISTWIDGET_H
#define BRICKLISTWIDGET_H

#include <QWidget>
#include <QLabel>
#include <QLineEdit>
#include <QVBoxLayout>
#include <string>
#include "brick_widget.h"

class brick_list_widget : public brick_widget {
 public:
     brick_list_widget(QWidget *parent = 0);

     virtual void add(const std::string &text, QPoint pos, QPoint offset);

     void add(brick_widget *dw);
     virtual std::string code();

 protected:

     void resize_down();
     void delete_up();
     void flip();

     QLineEdit *m_label;
     QVBoxLayout *m_main;
     QBoxLayout *m_container;
 };

 #endif
