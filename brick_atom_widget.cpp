#include <QtGui>
#include <QString>
#include <string>
#include <iostream>
#include "brick_atom_widget.h"
#include "sexpr.h"

using namespace std;

brick_atom_widget::brick_atom_widget(const string &code, QWidget *parent)
  : brick_widget(parent)
{
  // set depth based on parent depth
  brick_atom_widget *dwparent = static_cast<brick_atom_widget *>(parent);
  if (dwparent) m_depth=dwparent->get_depth()+1;

  m_main = new QVBoxLayout();
  setLayout(m_main);

  // build atom
  m_label = new QLineEdit(QString::fromStdString(code));
  m_label->setFrame(false);
  m_main->addWidget(m_label);
  // dont want to drop into an atom
  setAcceptDrops(false);

  setMinimumSize(0, 0);
  m_main->setSpacing(0);
  m_main->setMargin(0);
  m_main->setContentsMargins(10,5,5,5);
  unsigned int r=m_depth*5%255;
  unsigned int g=m_depth*20%255;
  unsigned int b=m_depth*30%255;
  setStyleSheet(QString("border: 0px solid black; color:white; background:rgb(%1,%2,%3);")
                .arg(r).arg(g).arg(b));

}
