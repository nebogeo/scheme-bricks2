 HEADERS     = brick_widget.h \
               brick_list_widget.h \
               brick_atom_widget.h \
               canvas_widget.h \
               sexpr.h \
               drag_widget.h
 RESOURCES   = schemebricks.qrc
 SOURCES     = brick_widget.cpp \
               brick_list_widget.cpp \
               brick_atom_widget.cpp \
               canvas_widget.cpp \
               drag_widget.cpp \
               sexpr.cpp \
               main.cpp
